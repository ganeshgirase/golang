package main

import (
    "fmt"
    "os"
    "syscall"
)

var KB = uint64(1024)

type DiskUsage struct {
    stat *syscall.Statfs_t
}

// Returns an object holding the disk usage of volumePath
// This function assumes volumePath is a valid path
func NewDiskUsage(volumePath string) *DiskUsage {

    var stat syscall.Statfs_t
    syscall.Statfs(volumePath, &stat)
    return &DiskUsage{&stat}
}

// Total free bytes on file system
func (self *DiskUsage) Free() uint64 {
    return self.stat.Bfree * uint64(self.stat.Bsize)
}

// Total available bytes on file system to an unpriveleged user
func (self *DiskUsage) Available() uint64 {
    return self.stat.Bavail * uint64(self.stat.Bsize)
}

// Total size of the file system
func (self *DiskUsage) Size() uint64 {
    return self.stat.Blocks * uint64(self.stat.Bsize)
}

// Total bytes used in file system
func (self *DiskUsage) Used() uint64 {
    return self.Size() - self.Free()
}

// Percentage of use on the file system
func (self *DiskUsage) Usage() float32 {
    return float32(self.Used()) / float32(self.Size())
}

func main() {

    wd, _ := os.Getwd()
    fmt.Println(wd)
    usage := NewDiskUsage(wd)
    fmt.Println("Free:", usage.Free()/(KB*KB))
    fmt.Println("Available:", usage.Available()/(KB*KB))
    fmt.Println("Size:", usage.Size()/(KB*KB))
    fmt.Println("Used:", usage.Used()/(KB*KB))
    fmt.Println("Usage:", usage.Usage()*100, "%")
}
